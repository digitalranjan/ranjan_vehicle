# -*- coding: utf-8 -*-

from odoo import fields, models, api, _

class Vehicle(models.Model):
    """Vehicle Master Object"""
    _name = 'vehicle.master'
    _rec_name = 'vehicle_id'
    _description = "Vehicle Details"

    vehicle_id = fields.Char(string='Vehicle ID', required=True, copy=False, readonly=True,
                           default=lambda self: _('New'))
    vech_ser_no = fields.Integer(string='Serial No')
    owner_id = fields.Many2one('res.partner', string='Owner Name ')
    vehicle_product = fields.Selection([('b', 'Bulk'), ('p', 'Packed')], string='Vehicle Product')
    image_1920 = fields.Image(string='Image', max_width=500, max_height=500)#, max_width=500, max_height=500
    image_1922 = fields.Image(string='Image ')
    image_1923 = fields.Image(string='Image ')
    image_1924 = fields.Image(string='Image ')
    image_1925 = fields.Image(string='Image ')
    # images_ids = fields.One2many('vehicle.image', 'image_id', string="Image")
    reg_no = fields.Selection([('license', 'License,'), ('permit', 'Permit'), ('certificate', 'Certificate')], string='Identification number')
    registration_license_type = fields.Selection([('safety', 'Safety training certification'),
                               ('general', 'General driving license'),
                               ('yard', 'Yard access'),
                               ('rack', 'Loading rack access')], string='License Type')
    address = fields.Many2one('res.partner', string='Address Number') # Contact Master in Odoo

    #Sql Constraints
    _sql_constraints = [
        ('vech_ser_no_uniq', 'unique(vech_ser_no)', 'Serial No Must Be Unique')
    ]

    @api.model
    def create(self, vals):
        print("#images >>>>>>::::: ", self.image_1920)
        if vals.get('vehicle_id', _('New')) == _('New'):
            vals['vehicle_id'] = self.env['ir.sequence'].next_by_code('vehicle.master') or _('New')

        return super().create(vals)

class VehicleImage(models.Model):
    _name = 'vehicle.image'
    _description = "Vehicle Details"

    # image_id = fields.Many2one('vehicle.master', string="Images")
