'''compute_digits()'''
"""i) create above function with default parameters for accepting two numbers and
pass the operator as parameter,"""
print("######compute_digits()#####")

import operator
def compute_digits(oper, a=4, b=5):

    return oper(a, b)

default_value = compute_digits(operator.add)
print("a + b form default parameter :", default_value)

new_value = compute_digits(operator.sub, a=50)
print("a override the default parameter :", new_value)