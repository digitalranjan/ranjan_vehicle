# OOP Exercise 1: Create a Class with instance attributes
# print("Exercise 1: Create a Class with instance attributes")
# class Vehicle:
#     def __init__(self, max_speed, mileage):
#         self.max_speed = max_speed
#         self.mileage = mileage
# modelX = Vehicle(240, 18)
# print(modelX.max_speed, modelX.mileage)

# class Car:
#     pass

"""
Exercise 3: Create a child class Bus that will inherit all of
            the variables and methods of the Vehicle class
"""
class Vehicle:
    def __init__(self, name, max_speed, mileage):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage

class Bus(Vehicle):
    pass

School_bus = Bus('School Volvo', 180, 12)
print("Vehicle Name: ", School_bus.name, "Speed:", School_bus.max_speed, "Mileage:", School_bus.mileage)

