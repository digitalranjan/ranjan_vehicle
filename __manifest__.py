############################################################################
# Bista Solutions
# Copyright (c) 2023 (http://www.bistasolutions.com)
############################################################################
{
    'name': 'Vehicle',
    'version': '16.0',
    'summary': 'Digital Ranjan ',
    'description': 'This is Test Module',
    'category': 'Training',
    'website': 'http://www.bistasolutions.com',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/vehicle_view.xml',
    ],
    'demo': [],
    'application': True,
    'license': 'LGPL-3',
    'installable': True,
    'auto-install': False,
}