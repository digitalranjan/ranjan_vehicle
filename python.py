"""a) reverse_words()"""
# Input: “My name is Rahul”
# Output: “Rahul is name My”

def reverse_words(string):
    # reversing words in a given string
    str = string.split()[::-1]
    rev_str = []
    for word in str:
        # appending reversed words to l
        rev_str.append(word)

    # printing reverse words
    print(" ".join(rev_str))

# input string
string = "My name is Ranjan"
reverse_words(string)

"""b) reverse_list()"""
# Input: [4,3,2,1,5,6]
# Output: [6,5,1,2,3,4]

def reverse_list(lst):
    new_list = lst[::-1]
    return new_list

lst = [4, 3, 2, 1, 5, 6]
print("list is: ", lst)
print("Reverse: ", reverse_list(lst))


"""c) compute_digits()"""
def compute_digits( a = 7,  b = 8):
    sum = a + b
    print('Sum:', sum)


compute_digits(2, 3)

compute_digits(a = 2)

"""
# challenges 
1. image Field
2. Field / Contact Master in Odoo / reg_no... / owner... 
3. python  compute_digits

Python Compute Program  (Class > Function)
 # the default parameter.
 # new parameter values passed
"""
###################################################
###################################################
'''## mapping Image Fields
        model.Model
    #In your model add
    your_images_ids = fields.One2many(“your.image.model”, “yourmodel_id”)
    
    #in your.image.model (new one) model:
    
    … other fields..
    yourmodel_id = fields.Many2one(“your.model”)
    image = fields.Binary()
    
      <!-- options='{"preview_image": "image_medium"}'-->
      image_1920 | image_1024 |image_512 | image_256 | image_128

'''
"""
Many2many is one of the relation types in databases.
 There are 3 types: 
 One2many – each class has teacher;
 Many2one – each class has list of students but student can’t be in more than one class. 
        ### And two sided relation: 
 Many2many. All students have list of exams to pass and exams are not enclosed to one student.

#All students have list of exams to pass and exams are not enclosed to one student.
"""
"""
# from dateutil.relativedelta import relativedelta
# 
# age = fields.Integer(string="Age", compute="_calculate_age")
# 
#    @api.depend('date_of_birth')
#     def _calculate_age(self):
# 
#           if self.date_of_birth:
# 
#              d1 = self.date_of_birth
# 
#             d2 = datetime.date.today()
# 
#            self.age = relativedelta(d2, d1).years


"""



